package cl.ubb.NumeroPerfecto;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

public class NumeroPerfectoTest {

	@Test
	public void IngresarSeisRetornarPerfectoVerdadero() {
		/*arrange*/
		NumeroPerfecto perfecto = new NumeroPerfecto();
		boolean resultado;
		
		/*act*/
		resultado= perfecto.DeterminarNumeroPerfecto(6);
		
		/*assert*/
		assertThat(resultado,is(true));
			
		
		
		
	}
	
	
	
	@Test
	public void IngresarVeintiochoRetornarPerfectoVerdadero(){
		/*arrange*/
		NumeroPerfecto perfecto = new NumeroPerfecto();
		boolean resultado;
		
		/*act*/
		resultado = perfecto.DeterminarNumeroPerfecto(28);
		
		/*assert*/
		assertThat(resultado,is(true));
	}
	
	
	
	@Test
	public void IngresarCuatroNueveSeisRetornarPerfectoVerdadero(){
		/*arrange*/
		NumeroPerfecto perfecto = new NumeroPerfecto();
		boolean resultado;
		
		/*act*/
		resultado = perfecto.DeterminarNumeroPerfecto(496);
		
		/*assert*/
		assertThat(resultado,is(true));
	
	}
	
	
	@Test
	public void IngresarQuinientosRetornaPerfectoVerdadero(){
		/*arrange*/
		NumeroPerfecto perfecto = new NumeroPerfecto();
		boolean resultado;
		
		/*act*/
		resultado = perfecto.DeterminarNumeroPerfecto(500);
		
		/*assert*/
		assertThat(resultado,is(true));
	
	}

}
